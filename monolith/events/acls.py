import requests
import json

from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    url = "https://api.pexels.com/v1/search"
    headers = {"Authorization": PEXELS_API_KEY}
    payload = {
        "query": f"{city} {state}",
        "orientation": "landscape",
        "size": "large",
        "locale": "en-US",
        "per_page": "1",
    }
    r = requests.get(
        url,
        headers=headers,
        params=payload,
    )
    content = json.loads(r.content)

    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}


def get_weather_data(city, state):
    coord_url = "http://api.openweathermap.org/geo/1.0/direct"
    coord_payload = {
        "q": f"{city},{state},US",
        "appid": OPEN_WEATHER_API_KEY,
        "limit": 5,
    }
    coord_r = requests.get(
        coord_url,
        params=coord_payload,
    )
    coord_content = json.loads(coord_r.content)
    lat, lon = coord_content[0]["lat"], coord_content[0]["lon"]

    url = "https://api.openweathermap.org/data/2.5/weather"
    payload = {
        "lat": lat,
        "lon": lon,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
        "lang": "en",
    }
    r = requests.get(
        url,
        params=payload,
    )
    content = json.loads(r.content)

    try:
        return {
            "weather": {
                "temp": content["main"]["temp"],
                "description": content["weather"][0]["description"],
            }
        }
    except (KeyError, IndexError):
        return {"weather": None}
