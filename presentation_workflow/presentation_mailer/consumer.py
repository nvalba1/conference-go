import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()

while True:
    try:

        def process_approval(ch, method, properties, body):
            approval_body = json.loads(body)
            send_mail(
                subject="Your presentation has been accepted",
                message=f"{approval_body['presenter_name']}, we're happy to tell you that your presentation {approval_body['title']} has been accepted",
                from_email="admin@conference.go",
                recipient_list=[approval_body["presenter_email"]],
                fail_silently=False,
            )

        def process_rejection(ch, method, properties, body):
            rejection_body = json.loads(body)
            send_mail(
                subject="Your presentation has been not accepted",
                message=f"{rejection_body['presenter_name']}, we regret to inform you that your presentation {rejection_body['title']} has not been accepted",
                from_email="admin@conference.go",
                recipient_list=[rejection_body["presenter_email"]],
                fail_silently=False,
            )

        def main():
            parameters = pika.ConnectionParameters(host="rabbitmq")
            connection = pika.BlockingConnection(parameters)
            approval_channel = connection.channel()
            rejection_channel = connection.channel()
            approval_channel.queue_declare(queue="presentation_approvals")
            rejection_channel.queue_declare(queue="presentation_rejections")
            approval_channel.basic_consume(
                queue="presentation_approvals",
                on_message_callback=process_approval,
                auto_ack=True,
            )
            rejection_channel.basic_consume(
                queue="presentation_rejections",
                on_message_callback=process_rejection,
                auto_ack=True,
            )

            approval_channel.start_consuming()
            rejection_channel.start_consuming()

        if __name__ == "__main__":
            try:
                main()
            except KeyboardInterrupt:
                print("Interrupted")
                try:
                    sys.exit(0)
                except SystemExit:
                    os._exit(0)

    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)
